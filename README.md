# Cookie-Editor+
(I'll give all of the source-code later lmao, too lazy right now)

[Cookie-Editor+](https://cookie-editor.com/) is a browser extension/add-on that lets you efficiently create, edit and delete cookies for the current tab. Perfect for developing, quickly testing or even manually managing your cookies for your privacy. Forked from [Moustachauve's Cookie-Editor](https://github.com/Moustachauve/cookie-editor)

## Description
Cookie-Editor+ is designed to have a simple to use interface that let you do most standard cookie operations quickly. It is ideal for developing and testing web pages. It has a few tweaks from [Moustachauve's Cookie-Editor](https://github.com/Moustachauve/cookie-editor) to make your experience *better*.

You can easily create, edit and delete a cookie for the current page that you are visiting.
There is also a handy button to mass delete all the cookies for the current page.

Cookie-Editor+ is available for Firefox, Google Chrome, Safari, and Edge. Opera is a virus so we're not supporting that. It should be possible to install it on any webkit browser, but keep in mind that only the previous four browsers are officially supported.

Cookie-Editor+ is available on Firefox for Android and Safari for iOS, with an interface optimised for touchscreens.

## Installation (Not working, just links to the unforked version. Please build yourself.)
### Install on Google Chrome
Find this extension on the [Chrome Web Store](https://chrome.google.com/webstore/detail/cookie-editor/hlkenndednhfkekhgcdicdfddnkalmdm?utm_campaign=github).
[![Chrome Web Store](readme/get-chrome.png)](https://chrome.google.com/webstore/detail/cookie-editor/hlkenndednhfkekhgcdicdfddnkalmdm?utm_campaign=github)

### Install on Firefox
Find this extension on the [Firefox Add-ons site](https://addons.mozilla.org/addon/cookie-editor?utm_campaign=external-github-readme).
[![Firefox Add-ons](readme/get-firefox.webp)](https://addons.mozilla.org/addon/cookie-editor?utm_campaign=external-github-readme)

### Install on Safari
Cookie-Editor is available for both Mac and iOS. It has been tested on Mac, iPhone and iPad.  
Find this extension on the [App Store](https://apps.apple.com/app/apple-store/id6446215341?pt=126143671&ct=github&mt=8).  
[![Apple App Store](readme/get-safari-mac.svg)](https://apps.apple.com/app/apple-store/id6446215341?pt=126143671&ct=github&mt=8)

### Install on Microsoft Edge
Find this extension on the [Microsoft Store](https://microsoftedge.microsoft.com/addons/detail/cookieeditor/neaplmfkghagebokkhpjpoebhdledlfi).

## Feature Suggestions or Bug Reports
To submit a feature suggestion or file a bug report, please [create a new issue here](https://github.com/Moustachauve/cookie-editor/issues).

## How to build

1. Run npm install to make sure you have all the required packages installed.
2. Run the command `grunt`
3. All the files are in the `dist` directory created

### Note for Safari

Safari needs to be built in Xcode. I have only tested building Cookie-Editor on Xcode 15.


## Disclaimer

This project is not an official Google project. It is not supported by
Google and Google specifically disclaims all warranties as to its quality,
merchantability, or fitness for a particular purpose.
